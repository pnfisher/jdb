ZIP ?= /usr/lib/jvm/openjdk-7/src.zip
DIR = com/sun/tools/example/debug/tty
PKG = $(subst /,.,$(DIR))
CP = $(JAVA_HOME)/lib/tools.jar
PRG = jdb
JAR = $(PRG).jar
MANIFEST = Manifest.txt
SRC = $(wildcard $(DIR)/*.java)
CLASSES = $(subst java,class,$(SRC))
#SHELL = /bin/sh -x

ifeq ($(JAVA_HOME),)
$(error "JAVA_HOME isn't set")
endif

ifeq ($(shell test -f $(ZIP) && echo x),)
$(error "can't find $(ZIP)")
endif

$(PRG): patch $(JAR)
	@echo creating $(PRG)
	@rm -rf $@
	@echo '#!/bin/sh' > $@
	@echo \
	'[ -z $${JAVA_HOME} ] && echo "error, JAVA_HOME not set" 1>&2 && exit 1' \
	>> $@
	@echo 'exec $${JAVA_HOME}/bin/java $$JAVA_ARGS -jar $$0 "$$@"' >> $@
	@echo "exit 1" >> $@
	@cat $(JAR) >> $@
	@chmod ugo+x $@

.PHONY: install
install: $(PRG)
	@if [ -d $(HOME)/bin ]; then \
		echo ln -sf $(CURDIR)/$(PRG) $(HOME)/bin/myjdb; \
		ln -sf $(CURDIR)/$(PRG) $(HOME)/bin/myjdb; \
	else \
		echo $(HOME)/bin doesn\'t exist, skipping install; \
	fi

.PHONY: unzip
ifeq ($(SRC),)
patch: unzip
	@patch -Np1 -d com < $(PRG).patch
	$(MAKE) compile
else
patch:
	@echo skipping unzip and patch
endif

$(PRG).patch: clean
	rm -rf patched $@
	mv com patched
	$(MAKE) unzip
	diff -Naur com patched > $@ || exit 0
	rm -rf com
	mv patched com

unzip:
	@echo unzipping $(ZIP)
	@unzip -qo $(ZIP) com/sun/tools/example/debug/tty/*.java

$(JAR): $(MANIFEST) $(CLASSES)
	jar cfem $(JAR) $(PKG).TTY $< $(DIR)/*.class

%.class: %.java
	$(JAVA_HOME)/bin/javac -cp $(CP):. $<

.PHONY: compile classes
compile classes:
	$(JAVA_HOME)/bin/javac -cp $(CP):. $(DIR)/*.java

$(MANIFEST):
	rm -rf $@
	echo "Class-Path: $(JAVA_HOME)/lib/tools.jar" > $@

.PHONY: clean
clean:
	rm -rf $(DIR)/*.class $(JAR) $(MANIFEST)

.PHONY: cleanall
cleanall: clean
	rm -rf com $(PRG)
